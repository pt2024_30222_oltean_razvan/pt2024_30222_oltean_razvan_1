package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class PolynomialView extends JFrame{
    JPanel mainPanel = new JPanel();
    JPanel inputPanel = new JPanel(new GridLayout(2,2));
    JLabel firstPolynomialLabel = new JLabel("Insereaza primul polinom:");
    JTextField firstPolynomialField = new JTextField();
    JLabel secondPolynomialLabel = new JLabel("Insereaza al doilea polinom:");
    JTextField secondPolynomialField = new JTextField();
    JPanel operationsPanel = new JPanel();
    JButton addButton = new JButton("Adunare");
    JButton subtractButton = new JButton("Scadere");
    JButton multiplyButton = new JButton("Inmultire");
    JButton divideButton = new JButton("Impartire");
    JButton derivativeButton = new JButton("Deriveaza");
    JButton integralButton = new JButton("Integreaza");
    JPanel resultPanel = new JPanel();
    JLabel firstResultLabel = new JLabel("Rezultat:");
    JTextField firstResultField = new JTextField();
    JLabel secondResultLabel = new JLabel("Rezultat(Optional):");
    JTextField secondResultField = new JTextField();

    public PolynomialView(){
        firstPolynomialField.setColumns(25);
        secondPolynomialField.setColumns(25);
        inputPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        inputPanel.add(firstPolynomialLabel);
        inputPanel.add(firstPolynomialField);
        inputPanel.add(secondPolynomialLabel);
        inputPanel.add(secondPolynomialField);


        operationsPanel.add(addButton);
        operationsPanel.add(subtractButton);
        operationsPanel.add(multiplyButton);
        operationsPanel.add(divideButton);
        operationsPanel.add(derivativeButton);
        operationsPanel.add(integralButton);
        operationsPanel.setLayout(new GridLayout(3,2));
        operationsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        firstResultField.setColumns(25);
        secondResultField.setColumns(25);
        resultPanel.add(firstResultLabel);
        resultPanel.add(firstResultField);
        resultPanel.add(secondResultLabel);
        resultPanel.add(secondResultField);
        resultPanel.setLayout(new GridLayout(2,2));
        resultPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        mainPanel.add(inputPanel);
        mainPanel.add(operationsPanel);
        mainPanel.add(resultPanel);
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));

        this.add(mainPanel);
        this.setContentPane(mainPanel);
        this.setTitle("Calculator Polinomial - Oltean Razvan Marian");
        this.setSize(550, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        firstResultField.setEditable(false);
        secondResultField.setEditable(false);

        inputPanel.setBackground(new Color(6, 172, 255));
        operationsPanel.setBackground(Color.CYAN);
        resultPanel.setBackground(Color.CYAN);

        Font labelFont = new Font("Impact", Font.PLAIN, 16);
        Font buttonFont = new Font("Impact", Font.PLAIN, 12);

        firstPolynomialLabel.setFont(labelFont);
        secondPolynomialLabel.setFont(labelFont);
        firstResultLabel.setFont(labelFont);
        secondResultLabel.setFont(labelFont);

        addButton.setFont(buttonFont);
        addButton.setBackground(Color.YELLOW);
        subtractButton.setFont(buttonFont);
        subtractButton.setBackground(Color.YELLOW);
        multiplyButton.setFont(buttonFont);
        multiplyButton.setBackground(Color.ORANGE);
        divideButton.setFont(buttonFont);
        divideButton.setBackground(Color.ORANGE);
        derivativeButton.setFont(buttonFont);
        derivativeButton.setBackground(Color.RED);
        integralButton.setFont(buttonFont);
        integralButton.setBackground(Color.RED);
    }
    public String getFirstPolynom(){
        return firstPolynomialField.getText();
    }
    public String getSecondPolynom(){
        return secondPolynomialField.getText();
    }
    public void setFirstResult(String res){
        firstResultField.setText(res);
    }
    public void setSecondResult(String res){
        secondResultField.setText(res);
    }
    public void showError(String err){
        JOptionPane.showMessageDialog(this, err);
    }
    public void makeAdditionListener(ActionListener act){
        addButton.addActionListener(act);
    }
    public void makeSubtractionListener(ActionListener act){
        subtractButton.addActionListener(act);
    }
    public void makeMultiplicationListener(ActionListener act){
        multiplyButton.addActionListener(act);
    }
    public void makeDivisionListener(ActionListener act){
        divideButton.addActionListener(act);
    }
    public void makeDerivationListener(ActionListener act){
        derivativeButton.addActionListener(act);
    }
    public void makeIntegrationListener(ActionListener act){
        integralButton.addActionListener(act);
    }
    public void clearResults(){
        firstResultField.setText("");
        secondResultField.setText("");
    }
}
