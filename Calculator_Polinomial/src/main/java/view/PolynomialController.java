package view;

import model.PolynomialModel;
import org.example.DivisionException;
import logic.PolynomialOperations;
import org.example.ZeroDivisionException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PolynomialController {
    private PolynomialView view;
    private PolynomialModel firstPolynomial;
    private PolynomialModel secondPolynomial;
    public PolynomialController(PolynomialView view){
        this.view = view;
        firstPolynomial = new PolynomialModel();
        secondPolynomial = new PolynomialModel();
        view.makeAdditionListener(new AdditionListener());
        view.makeSubtractionListener(new SubtractionListener());
        view.makeMultiplicationListener(new MultiplicationListener());
        view.makeDivisionListener(new DivisionListener());
        view.makeDerivationListener(new DerivationListener());
        view.makeIntegrationListener(new IntegrationListener());
    }
    class AdditionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            try{
                view.clearResults();
                firstPolynomial.getMonomials().clear();
                secondPolynomial.getMonomials().clear();
                String firstPol = view.getFirstPolynom();
                String secondPol = view.getSecondPolynom();
                firstPolynomial = new PolynomialModel(firstPol);
                secondPolynomial = new PolynomialModel(secondPol);
                PolynomialModel result = PolynomialOperations.addition(firstPolynomial,secondPolynomial);
                String resultString = result.toString();
                if(resultString.startsWith("+")) {
                    resultString = resultString.substring(1);
                }
                view.setFirstResult(resultString);
            }catch (Exception ex){
                view.showError("Bad input");
            }
        }
    }
    class SubtractionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            try{
                view.clearResults();
                firstPolynomial.getMonomials().clear();
                secondPolynomial.getMonomials().clear();
                String firstPol = view.getFirstPolynom();
                String secondPol = view.getSecondPolynom();
                firstPolynomial = new PolynomialModel(firstPol);
                secondPolynomial = new PolynomialModel(secondPol);
                PolynomialModel result = PolynomialOperations.subtraction(firstPolynomial,secondPolynomial);
                String resultString = result.toString();
                if(resultString.startsWith("+")) {
                    resultString = resultString.substring(1);
                }
                view.setFirstResult(resultString);
            }catch (Exception ex){
                view.showError("Bad input");
            }
        }
    }
    class MultiplicationListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            try{
                view.clearResults();
                firstPolynomial.getMonomials().clear();
                secondPolynomial.getMonomials().clear();
                String firstPol = view.getFirstPolynom();
                String secondPol = view.getSecondPolynom();
                firstPolynomial = new PolynomialModel(firstPol);
                secondPolynomial = new PolynomialModel(secondPol);
                PolynomialModel result = PolynomialOperations.multiplication(firstPolynomial,secondPolynomial);
                String resultString = result.toString();
                if(resultString.startsWith("+")) {
                    resultString = resultString.substring(1);
                }
                view.setFirstResult(resultString);
            }catch (Exception ex){
                view.showError("Bad input");
            }
        }
    }
    class DivisionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            try{
                view.clearResults();
                firstPolynomial.getMonomials().clear();
                secondPolynomial.getMonomials().clear();
                String firstPol = view.getFirstPolynom();
                String secondPol = view.getSecondPolynom();
                firstPolynomial = new PolynomialModel(firstPol);
                secondPolynomial = new PolynomialModel(secondPol);
                if(secondPol == null || secondPol.equals("0") || secondPol.isBlank()) throw new ZeroDivisionException();
                if(firstPolynomial.getMonomials().lastKey() < secondPolynomial.getMonomials().lastKey()) throw new DivisionException();
                PolynomialModel result[] = PolynomialOperations.division(firstPolynomial,secondPolynomial);
                String firstResultString = result[0].toString();
                String secondResultString = result[1].toString();
                if(firstResultString.startsWith("+")) {
                    firstResultString = firstResultString.substring(1);
                }
                if(secondResultString.startsWith("+")) {
                    secondResultString = secondResultString.substring(1);
                }
                view.setFirstResult(firstResultString);
                view.setSecondResult(secondResultString);
            }catch(Exception ex){
                view.showError("Bad input");
            }catch(DivisionException dx){
                view.showError("Nu poti impartii un polinom cu un grad mai mic la un polinom cu un grad mai mare");
            }catch(ZeroDivisionException zx){
                view.showError("Nu poti impartii un polinom la '0'");
            }
        }
    }
    class DerivationListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            try{
                view.clearResults();
                firstPolynomial.getMonomials().clear();
                secondPolynomial.getMonomials().clear();
                String firstPol = view.getFirstPolynom();
                String secondPol = view.getSecondPolynom();
                firstPolynomial = new PolynomialModel(firstPol);
                secondPolynomial = new PolynomialModel(secondPol);
                PolynomialModel firstResult = PolynomialOperations.derivation(firstPolynomial);
                PolynomialModel secondResult = PolynomialOperations.derivation(secondPolynomial);
                String firstResultString = firstResult.toString();
                String secondResultString = secondResult.toString();
                if(firstResultString.startsWith("+")) {
                    firstResultString = firstResultString.substring(1);
                }
                if(secondResultString.startsWith("+")) {
                    secondResultString = secondResultString.substring(1);
                }
                view.setFirstResult(firstResultString);
                view.setSecondResult(secondResultString);
            }catch (Exception ex){
                view.showError("Bad input");
            }
        }
    }
    class IntegrationListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            try{
                view.clearResults();
                firstPolynomial.getMonomials().clear();
                secondPolynomial.getMonomials().clear();
                String firstPol = view.getFirstPolynom();
                String secondPol = view.getSecondPolynom();
                firstPolynomial = new PolynomialModel(firstPol);
                secondPolynomial = new PolynomialModel(secondPol);
                PolynomialModel firstResult = PolynomialOperations.integration(firstPolynomial);
                PolynomialModel secondResult = PolynomialOperations.integration(secondPolynomial);
                String firstResultString = firstResult.toString();
                String secondResultString = secondResult.toString();
                if(firstResultString.equals("0")){
                    firstResultString = "C";
                }else{
                    firstResultString += "+C";
                }
                if(secondResultString.equals("0")){
                    secondResultString = "C";
                }else{
                    secondResultString += "+C";
                }
                if(firstResultString.startsWith("+")) {
                    firstResultString = firstResultString.substring(1);
                }
                if(secondResultString.startsWith("+")) {
                    secondResultString = secondResultString.substring(1);
                }
                view.setFirstResult(firstResultString);
                view.setSecondResult(secondResultString);
            }catch (Exception ex){
                view.showError("Bad input");
            }
        }
    }

}
