package logic;

import model.PolynomialModel;

public class PolynomialOperations {
    public static PolynomialModel addition(PolynomialModel firstPolynomial, PolynomialModel secondPolynomial){
        PolynomialModel aux = new PolynomialModel();
        for(Integer degree:firstPolynomial.getMonomials().keySet()){
            double coefficient = firstPolynomial.getMonomials().get(degree);
            aux.addMonomial(degree,coefficient);
        }
        for(Integer degree:secondPolynomial.getMonomials().keySet()){
            double secondCoefficient = secondPolynomial.getMonomials().get(degree);
            if(aux.getMonomials().containsKey(degree)) {
                double firstCoefficient = aux.getMonomials().get(degree);
                aux.addMonomial(degree, secondCoefficient + firstCoefficient);
            }else{
                aux.addMonomial(degree,secondCoefficient);
            }
        }
        return aux;
    }
    public static PolynomialModel subtraction(PolynomialModel firstPolynomial,PolynomialModel secondPolynomial){
        PolynomialModel aux = new PolynomialModel();
        for(Integer degree:firstPolynomial.getMonomials().keySet()){
            double coefficient = firstPolynomial.getMonomials().get(degree);
            aux.addMonomial(degree,coefficient);
        }
        for(Integer degree:secondPolynomial.getMonomials().keySet()){
            double secondCoefficient = secondPolynomial.getMonomials().get(degree);
            secondCoefficient = -secondCoefficient;
            if(aux.getMonomials().containsKey(degree)) {
                double firstCoefficient = aux.getMonomials().get(degree);
                aux.addMonomial(degree, secondCoefficient + firstCoefficient);
            }else{
                aux.addMonomial(degree,secondCoefficient);
            }
        }
        return aux;
    }
    public static PolynomialModel multiplication(PolynomialModel firstPolynomial,PolynomialModel secondPolynomial){
        PolynomialModel aux = new PolynomialModel();
        for(Integer firstDegree:firstPolynomial.getMonomials().keySet()){
            for(Integer secondDegree:secondPolynomial.getMonomials().keySet()){
                double firstCoefficient = firstPolynomial.getMonomials().get(firstDegree);
                double secondCoefficient = secondPolynomial.getMonomials().get(secondDegree);
                int resultDegree = firstDegree + secondDegree;
                double resultCoefficient = firstCoefficient * secondCoefficient;
                if(aux.getMonomials().containsKey(resultDegree)){
                    double bufferCoefficient = aux.getMonomials().get(resultDegree);
                    aux.addMonomial(resultDegree,bufferCoefficient + resultCoefficient);
                }else{
                    aux.addMonomial(resultDegree,resultCoefficient);
                }
            }
        }
        return aux;
    }
    public static PolynomialModel[] division(PolynomialModel firstPolynomial,PolynomialModel secondPolynomial){
        PolynomialModel[] aux = new PolynomialModel[2]; //aux[0] = Q, aux[1] = R
        aux[0] = new PolynomialModel();
        aux[1] = new PolynomialModel();
        int firstDegree = firstPolynomial.getMonomials().lastKey();
        int secondDegree = secondPolynomial.getMonomials().lastKey();
        double firstCoefficient = firstPolynomial.getMonomials().get(firstDegree);
        double secondCoefficient = secondPolynomial.getMonomials().get(secondDegree);
        PolynomialModel remainder = new PolynomialModel();
        do{
            remainder.getMonomials().clear();
            int quotientDegree = firstDegree - secondDegree;
            double quotientCoefficient = firstCoefficient / secondCoefficient;
            aux[0].addMonomial(quotientDegree, quotientCoefficient);
            for(Integer bufferDegree:secondPolynomial.getMonomials().descendingKeySet()){
                double bufferCoefficient = secondPolynomial.getMonomials().get(bufferDegree);
                remainder.addMonomial(quotientDegree + bufferDegree,quotientCoefficient * bufferCoefficient);
            }
            firstPolynomial = PolynomialOperations.subtraction(firstPolynomial,remainder);
            firstPolynomial.getMonomials().remove(firstDegree);
            if(firstPolynomial.getMonomials().isEmpty()){
                break;
            }else{
                firstDegree = firstPolynomial.getMonomials().lastKey();
            }
            firstCoefficient = firstPolynomial.getMonomials().get(firstDegree);
        }while((firstDegree >= secondDegree) && !firstPolynomial.getMonomials().isEmpty());
        if(firstPolynomial.getMonomials().isEmpty()) {
            aux[1].addMonomial(0, 0);
        }else aux[1] = firstPolynomial;
        return aux;
    }
    public static PolynomialModel derivation(PolynomialModel polynomial){
        PolynomialModel aux = new PolynomialModel();
        for(Integer degree:polynomial.getMonomials().keySet()){
            double coefficient = polynomial.getMonomials().get(degree);
            if(degree == 0){
                aux.getMonomials().remove(0);
            }else{
                aux.addMonomial(degree - 1,coefficient * degree);
            }
        }
        return aux;
    }
    public static PolynomialModel integration(PolynomialModel polynomial){
        PolynomialModel aux = new PolynomialModel();
        for(Integer degree:polynomial.getMonomials().descendingKeySet()){
            double coefficient = polynomial.getMonomials().get(degree);
            aux.addMonomial(degree + 1,coefficient / (degree + 1));
        }
        return aux;
    }
}
