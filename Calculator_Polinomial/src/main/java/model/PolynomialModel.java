package model;

import java.util.*;
import java.util.regex.*;

public class PolynomialModel {
        private TreeMap<Integer,Double> monomials = new TreeMap<>();
        public PolynomialModel(){}
        public PolynomialModel(String str) throws Exception{
                String pattern = "([+-])?(\\d*)(x)?(\\^)?(\\d*)\\s*";
                Pattern regex = Pattern.compile(pattern);
                Matcher match = regex.matcher(str);
                validateInput(str,pattern);
                while(match.find()) {
                        int degree;
                        int coefficient;
                        StringBuilder aux = new StringBuilder();
                        if (match.group(1) == null && match.group(2).isEmpty() && match.group(3) == null &&
                                match.group(4) == null && match.group(5).isEmpty()) {
                                continue;
                        }
                        if (match.group(1) == null) {
                                aux.append("+");
                                if (match.group(2).isEmpty()) {
                                        aux.append("1");
                                } else {
                                        aux.append(match.group(2));
                                }
                        } else {
                                if (match.group(2).isEmpty()) {
                                        aux.append(match.group(1)).append("1");
                                } else {
                                        aux.append(match.group(1)).append(match.group(2));
                                }
                        }
                        coefficient = Integer.parseInt(aux.toString());
                        aux.setLength(0);
                        if (match.group(3) != null) {
                                if (match.group(4) != null) {
                                        aux.append(match.group(5));
                                } else {
                                        aux.append("1");
                                }
                        } else {
                                aux.append("0");
                        }
                        if (!aux.toString().isBlank())
                                degree = Integer.parseInt(aux.toString());
                        else degree = 0;
                        aux.setLength(0);
                        if(getMonomials().containsKey(degree)){
                                this.addMonomial(degree, getMonomials().get(degree) + coefficient);
                        }else{
                                this.addMonomial(degree,coefficient);
                        }
                }
        }
        public void addMonomial(int degree,double coefficient){
                monomials.put(degree,coefficient);
        }
        public String toString(){
                StringBuilder aux = new StringBuilder();
                for(Integer degree: monomials.descendingKeySet()){
                        Double coefficient = monomials.get(degree);
                        String formattedValue;
                        if(coefficient == Math.floor(coefficient)){
                                formattedValue = String.format("%.0f",coefficient);
                        }else{
                                formattedValue = String.format("%.2f",coefficient);
                        }
                        if(coefficient > 0){
                                if(degree != 0){
                                        if(coefficient == 1){
                                                if(degree == 1){
                                                        aux.append("+x");
                                                }else{
                                                        aux.append("+").append("x^").append(degree);
                                                }
                                        }else{
                                                if(degree == 1){
                                                        aux.append("+").append(formattedValue).append("x");
                                                }else{
                                                        aux.append("+").append(formattedValue).append("x^").append(degree);
                                                }
                                        }
                                }else{
                                        aux.append("+").append(formattedValue);

                                }
                                if(!aux.isEmpty()) aux.append(" ");
                        }else if(coefficient < 0){
                                if(degree != 0){
                                        if(coefficient == -1){
                                                if(degree == 1){
                                                        aux.append("-").append("x");
                                                }else{
                                                        aux.append("-").append("x^").append(degree);
                                                }
                                        }else{
                                                if(degree == 1){
                                                        aux.append(formattedValue).append("x");
                                                }else{
                                                        aux.append(formattedValue).append("x^").append(degree);
                                                }
                                        }
                                }else{
                                        aux.append(formattedValue);
                                }
                                if(!aux.isEmpty()) aux.append(" ");
                        }
                }
                if(aux.isEmpty()) aux.append("0");
                return aux.toString();
        }
        public TreeMap<Integer, Double> getMonomials() {
                return monomials;
        }
        public void validateInput(String inputString,String validString) throws Exception{
                String invalidString = "[^" + validString + "]";
                Pattern invalidPattern = Pattern.compile(invalidString);
                Matcher invalidMatch = invalidPattern.matcher(inputString);
                if(invalidMatch.find()){
                        throw new Exception();
                }
                Pattern validPattern = Pattern.compile(validString);
                Matcher validMatch = validPattern.matcher(inputString);
                if(validMatch.find()) {
                        while (validMatch.find()) {
                                if(validMatch.group(1) == null && validMatch.group(2).isEmpty() && validMatch.group(3) == null && validMatch.group(4) == null && validMatch.group(5).isEmpty()){
                                        continue;
                                }
                                if (!validMatch.group().startsWith("+") && !validMatch.group().startsWith("-")){
                                        throw new Exception();
                                }
                        }
                }
        }
}
