package org.example;
import view.PolynomialController;
import view.PolynomialView;

public class PolynomialMVC {
    public static void main(String[] args) {
        PolynomialView view = new PolynomialView();
        PolynomialController controller = new PolynomialController(view);
        view.setVisible(true);
    }
}
