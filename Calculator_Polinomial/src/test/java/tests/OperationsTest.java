package tests;
import logic.PolynomialOperations;
import model.PolynomialModel;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class OperationsTest {
    @Test
    public void addTest1(){
        try{
            assertEquals(new PolynomialModel("x^2 +2x -3").toString(), PolynomialOperations.addition
                    (new PolynomialModel("-3x^2 -4x +1"),new PolynomialModel("4x^2 +6x -4")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void addTest2(){
        try{
            assertEquals(new PolynomialModel("x^3 +5x +1").toString(),PolynomialOperations.addition
                    (new PolynomialModel("-3x^2 -4x +1"),new PolynomialModel("x^3 +3x^2 +9x")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void subTest1(){
        try{
            assertEquals(new PolynomialModel("x^2 +2x -3").toString(),PolynomialOperations.subtraction
                    (new PolynomialModel("-3x^2 -4x +1"),new PolynomialModel("-4x^2 -6x +4")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void subTest2(){
        try{
            assertEquals(new PolynomialModel("x^3 +5x +1").toString(),PolynomialOperations.subtraction
                    (new PolynomialModel("2x^2 +6x -1"),new PolynomialModel("-x^3 +2x^2 +x -2")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void mulTest1(){
        try{
            assertEquals(new PolynomialModel("2x^3+2x+x^2+1").toString(),PolynomialOperations.multiplication
                    (new PolynomialModel("2x+1"),new PolynomialModel("x^2 +1")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void mulTest2(){
        try{
            assertEquals(new PolynomialModel("2x^3-2x").toString(),PolynomialOperations.multiplication
                    (new PolynomialModel("x-1"),new PolynomialModel("2x^2 +2x")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void divTest1(){
        try{
            PolynomialModel result[] = PolynomialOperations.division
                    (new PolynomialModel("4x^2 -2x +1"),new PolynomialModel("x-1"));
            assertEquals(new PolynomialModel("4x +2").toString(),result[0].toString());
            assertEquals(new PolynomialModel("3").toString(),result[1].toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void divTest2(){
        try{
            PolynomialModel result[] = PolynomialOperations.division
                    (new PolynomialModel("5x^3 -3x^2 +2x -1"),new PolynomialModel("x^2 +x -1"));
            assertEquals(new PolynomialModel("5x -8").toString(),result[0].toString());
            assertEquals(new PolynomialModel("15x -9").toString(),result[1].toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void derivTest1(){
        try{
            assertEquals(new PolynomialModel("3x^2 +2x -1").toString(),PolynomialOperations.derivation
                    (new PolynomialModel("x^3 +x^2 -x")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void derivTest2(){
        try{
            assertEquals(new PolynomialModel("42x^5 +10x^4").toString(),PolynomialOperations.derivation
                    (new PolynomialModel("7x^6 +2x^5")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void integrTest1(){
        try{
            assertEquals(new PolynomialModel("x^4 -x^3").toString(),PolynomialOperations.integration
                    (new PolynomialModel("4x^3 -3x^2")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void integrTest2(){
        try{
            assertEquals(new PolynomialModel("-8x^3 +x^2 +2x").toString(),PolynomialOperations.integration
                    (new PolynomialModel("-24x^2 +2 +2x")).toString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
